package be.thomasave.streamyoutube;


import android.os.Bundle;
import android.preference.PreferenceActivity;


public class settings extends AppCompatPreferenceActivity {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settingsactivity);
        addPreferencesFromResource(R.xml.preferences);

    }



}