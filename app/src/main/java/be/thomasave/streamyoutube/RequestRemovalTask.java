package be.thomasave.streamyoutube;

import android.os.AsyncTask;
import android.util.Log;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class RequestRemovalTask extends AsyncTask<Void, Void, Boolean> {

    int id;

    RequestRemovalTask(int requestid) {
        id=requestid;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        try {
            URL my_url = new URL("http://streamtube.thomasave.be/requestremoval/");
            HttpURLConnection httpConn = (HttpURLConnection) my_url.openConnection();
            httpConn.setRequestMethod("POST");
            httpConn.setDoInput(true);
            httpConn.setDoOutput(true);
            httpConn.setReadTimeout(20000000);
            httpConn.setConnectTimeout(20000000);

            OutputStream outputStream = httpConn.getOutputStream();
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream);
            BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);
            String parameters = "id=" + id;
            bufferedWriter.write(parameters);
            bufferedWriter.flush();
            bufferedWriter.close();
            outputStream.close();

            InputStream inputStream = httpConn.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String line = bufferedReader.readLine();
            bufferedReader.close();
            inputStream.close();

            httpConn.disconnect();
            if (line.equals("success")) {
                return true;
            } else {
                return false;
            }
        } catch (IOException e){
            e.printStackTrace();
            return false;
        }
    }

    @Override
    protected void onPostExecute(final Boolean success) {
        //NOOP
    }

    @Override
    protected void onCancelled() {
    }
}
