package be.thomasave.streamyoutube;


import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;
import java.io.File;
import java.io.IOException;


public class PlayingActivity extends AppCompatActivity {

    SeekBar seek_bar;
    int id = 0;
    ImageButton play_button, stop_button, repeat_button;
    final static MediaPlayer mediaPlayer = new MediaPlayer();
    Handler seekHandler = new Handler();
    static Boolean isNotification = true;
    Boolean isRepeating = true;
    ProgressDialog progress;
    Boolean download;
    String name = "No name found";
    DownloadManager manager;
    String url;
    String exists = "no";
    boolean isRetry = false;
    TextView songname;
    String option = "Download";
    static PowerManager.WakeLock mWakeLock;
    private static final int PERMISSION_REQUEST_CODE = 1;
    String basePath = Environment.DIRECTORY_MUSIC;
    ServiceConnection serviceConnection = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.playing_layout);
        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        mWakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                "streamyoutube:MediaPlayer");
        mWakeLock.acquire(10*60*1000L /*10 minutes*/);

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras != null) {
                id = extras.getInt("id");
                exists = extras.getString("exists");
            }
        } else {
            id = (int) savedInstanceState.getSerializable("id");
            exists = (String) savedInstanceState.getSerializable("exists");

        }
        play_button = findViewById(R.id.pauze);
        preparemediaplayer();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        Bundle extras = intent.getExtras();
        if (extras != null && extras.containsKey("Action")) {
            String action = (String) extras.get("Action");
            if (action != null) {
                if (action.equals("Resume")) {
                    updateButtons();
                    return;
                }
            }
        }
        id = extras != null ? extras.getInt("id") : 0;
        exists = extras != null ? extras.getString("exists") : null;
        preparemediaplayer();
        updateButtons();
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateButtons();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        updateButtons();
    }

    private void updateButtons() {
        if (mediaPlayer.isPlaying()) {
            play_button.setImageResource(R.drawable.ic_pause_black_48dp);
        } else {
            play_button.setImageResource(R.drawable.ic_play_arrow_black_48dp);
        }
    }

    void preparemediaplayer() {
        SharedPreferences sharedpreferences = getSharedPreferences("My_Prefs", Context.MODE_PRIVATE);
        name = sharedpreferences.getString("name", "No name found");
        SharedPreferences getoption = PreferenceManager.
                getDefaultSharedPreferences(getBaseContext());
        option = getoption.getString("download","Download");
        if (option.equals("Stream")) {
            download = false;
        } else {
            download = true;
            if (android.os.Build.VERSION.SDK_INT >= 23) {
                int result = getBaseContext().checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
                if (result != PackageManager.PERMISSION_GRANTED) {
                    this.requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
                    return;
                }
            }
        }
        if (exists.equals("no")) {
            if (id == 0) {
                RequestRemovalTask requestRemovalTask = new RequestRemovalTask(id);
                requestRemovalTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void) null);
                NotificationManager notificationManger =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManger.cancel(01);
                isNotification = false;
                finish();
            }

            url = "http://streamtube.thomasave.be/tmp/" + id + ".mp3";
            if (download) {
                progress = ProgressDialog.show(PlayingActivity.this, "",
                        "Downloading...", true);
                if (isFileExists()) {
                    playdownloaded();
                } else {
                    DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
                    request.setDestinationInExternalPublicDir(basePath + "/StreamTube/", name + ".mp3");
                    manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
                    request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_HIDDEN);
                    manager.enqueue(request);
                    registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
                }

            } else {
                mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                try {
                    progress = ProgressDialog.show(this, "",
                            "Buffering...", true);
                    mediaPlayer.setDataSource(url);
                    mediaPlayer.prepareAsync(); // might take long! (for buffering, etc)
                } catch (IOException e) {
                    e.printStackTrace();
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(PlayingActivity.this);
                    builder1.setMessage("There was some problem with the network, try again?");
                    builder1.setCancelable(false);
                    builder1.setPositiveButton(android.R.string.yes,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int Clickid) {
                                    dialog.cancel();
                                    isRetry = true;
                                    Intent i = new Intent(PlayingActivity.this, PlayingActivity.class);
                                    i.putExtra("id", id);
                                    i.putExtra("exists", exists);
                                    finish();
                                    startActivity(i);
                                }
                            });
                    builder1.setNegativeButton(android.R.string.no,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int Clickid) {
                                    dialog.cancel();
                                    RequestRemovalTask requestRemovalTask = new RequestRemovalTask(id);
                                    requestRemovalTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void) null);
                                    NotificationManager notificationManger =
                                            (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                                    notificationManger.cancel(01);
                                    isNotification = false;
                                    finish();
                                }
                            });
                    AlertDialog alert11 = builder1.create();
                    try {
                        alert11.show();}
                    catch (Exception b) {
                    }
                }
            }
        } else {
            progress = ProgressDialog.show(PlayingActivity.this, "",
                    "Preparing...", true);
            playdownloaded();
        }

        mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                progress.dismiss();
                getInit();
                seekUpdation();
                mediaPlayer.start();
                notification();
                mediaPlayer.setLooping(true);
                updateButtons();
            }
        });
        mediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                if(what == 100) {
                    preparemediaplayer();
                    return true;
                } else {
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(PlayingActivity.this);
                    builder1.setMessage("There was some problem with the network, try again?");
                    builder1.setCancelable(false);
                    builder1.setPositiveButton(android.R.string.yes,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int Clickid) {
                                    dialog.cancel();
                                    isRetry = true;
                                    Intent i = new Intent(PlayingActivity.this, PlayingActivity.class);
                                    i.putExtra("id", id);
                                    i.putExtra("exists", exists);
                                    finish();
                                    startActivity(i);
                                }
                            });
                    builder1.setNegativeButton(android.R.string.no,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int Clickid) {
                                    RequestRemovalTask requestRemovalTask = new RequestRemovalTask(id);
                                    requestRemovalTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void) null);
                                    NotificationManager notificationManger =
                                            (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                                    notificationManger.cancel(01);
                                    isNotification = false;
                                    finish();
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert11 = builder1.create();
                    try {
                        alert11.show();}
                    catch (Exception e) {
                    }
                    return false;
                }
            }
        });
    }
    private boolean isFileExists(){

        File folder1 = new File(Environment.getExternalStorageDirectory()+"/"+Environment.DIRECTORY_MUSIC +"/StreamTube/"+ name + ".mp3");
        return folder1.exists();


    }
    BroadcastReceiver onComplete=new BroadcastReceiver() {
        public void onReceive(Context ctxt, Intent intent) {
            playdownloaded();
    }
    };

    public void playdownloaded() {
        try {
            progress.dismiss();
            progress = ProgressDialog.show(PlayingActivity.this, "",
                    "Buffering...", true);
            String filePath = Environment.getExternalStorageDirectory()+"/"+Environment.DIRECTORY_MUSIC +"/StreamTube/"+ name + ".mp3";
            mediaPlayer.reset();
            mediaPlayer.setDataSource(filePath);
            mediaPlayer.prepareAsync(); // might take long! (for buffering, etc)
            RequestRemovalTask requestRemovalTask = new RequestRemovalTask(id);
            requestRemovalTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void) null);
        } catch (IOException e) {
            e.printStackTrace();
            AlertDialog.Builder builder1 = new AlertDialog.Builder(PlayingActivity.this);
            builder1.setMessage("There was some problem preparing the mediaplayer, try again?");
            builder1.setCancelable(false);
            builder1.setPositiveButton(android.R.string.yes,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int Clickid) {
                            dialog.cancel();
                            isRetry = true;
                            Intent i = new Intent(PlayingActivity.this, PlayingActivity.class);
                            i.putExtra("id", id);
                            i.putExtra("exists", exists);
                            finish();
                            startActivity(i);
                        }
                    });
            builder1.setNegativeButton(android.R.string.no,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int Clickid) {
                            dialog.cancel();
                            RequestRemovalTask requestRemovalTask = new RequestRemovalTask(id);
                            requestRemovalTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void) null);
                            NotificationManager notificationManger =
                                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                            notificationManger.cancel(01);
                            isNotification = false;
                            finish();
                        }
                    });
            AlertDialog alert11 = builder1.create();
            try {
                alert11.show();}
            catch (Exception a) {
            }
        }

    }

    public static void play() {
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.pause();
            mWakeLock.release();
        } else {
            mediaPlayer.start();
            mWakeLock.acquire(10*60*1000L /*10 minutes*/);
        }
    }

    public static void stop() {
        mediaPlayer.pause();
        if (mWakeLock.isHeld()) {
            mWakeLock.release();
        }
        mediaPlayer.seekTo(0);
    }

    public void getInit() {
        seek_bar = (SeekBar) findViewById(R.id.seekBar);
        play_button = (ImageButton) findViewById(R.id.pauze);
        stop_button = (ImageButton) findViewById(R.id.stop);
        repeat_button = (ImageButton) findViewById(R.id.repeat);
        songname = (TextView) findViewById(R.id.songname);
        songname.setText(name);
        seek_bar.setMax(mediaPlayer.getDuration());
        repeat_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isRepeating) {
                    mediaPlayer.setLooping(true);
                    isRepeating = true;
                    repeat_button.setImageResource(R.drawable.ic_repeat_black_48dp);
                } else {
                    mediaPlayer.setLooping(false);
                    isRepeating = false;
                    repeat_button.setImageResource(R.drawable.ic_repeat_white_48dp);
                }
            }
        });
        play_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mediaPlayer.isPlaying()) {
                    play_button.setImageResource(R.drawable.ic_play_arrow_black_48dp);
                } else {
                    if(!isNotification){
                        notification();
                    }
                    play_button.setImageResource(R.drawable.ic_pause_black_48dp);
                }
                play();
            }
        });
        stop_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                play_button.setImageResource(R.drawable.ic_play_arrow_black_48dp);
                NotificationManager notificationManger =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManger.cancel(01);
                isNotification = false;
                stop();
            }
        });
        seek_bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    mediaPlayer.seekTo(progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

    }
    Runnable run = new Runnable() {
        @Override public void run() {
            seekUpdation();
        }
    };
    public void seekUpdation() {
        seek_bar.setProgress(mediaPlayer.getCurrentPosition());
        seekHandler.postDelayed(run, 1000);
    }

    @Override
    public void onDestroy() {
        if(!isRetry && !exists.equals("yes")) {
            RequestRemovalTask requestRemovalTask = new RequestRemovalTask(id);
            requestRemovalTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void) null);
        }
        NotificationManager notificationManger =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManger.cancel(01);
        isNotification = false;
        mediaPlayer.stop();
        stopService(new Intent(this, KillService.class));
        seekHandler.removeCallbacks(run);
        if(mWakeLock.isHeld()) {
            mWakeLock.release();
        }
        super.onDestroy();
        if (this.serviceConnection != null) {
            unbindService(this.serviceConnection);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            preparemediaplayer();
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Please enable the 'Storage' permission. Otherwise, we can't save the song in your music folder.")
                        .setTitle("Permission not granted");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        PlayingActivity.this.requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        PlayingActivity.this.finish();
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        }
    }
    private void notification() {
        if (this.serviceConnection != null) {
            unbindService(this.serviceConnection);
        }
        serviceConnection = new ServiceConnection() {
            public void onServiceConnected(ComponentName className,
                                           IBinder binder) {
                ((KillService.KillBinder) binder).service.startService(new Intent(
                        PlayingActivity.this, KillService.class));

                // Stop Intent
                Intent stopIntent = new Intent(PlayingActivity.this, NotificationButtonIntentService.class);
                stopIntent.setAction(NotificationButtonIntentService.Action_Stop);
                PendingIntent stopPendingIntent = PendingIntent.getService(PlayingActivity.this, 0, stopIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                // Play Intent
                Intent toggleIntent = new Intent(PlayingActivity.this, NotificationButtonIntentService.class);
                toggleIntent.setAction(NotificationButtonIntentService.Action_Toggle);
                PendingIntent togglePendingIntent = PendingIntent.getService(PlayingActivity.this, 1, toggleIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                // Clicked Intent
                Intent clickedIntent = new Intent(PlayingActivity.this, PlayingActivity.class);
                clickedIntent.setAction(Intent.ACTION_MAIN);
                clickedIntent.addCategory(Intent.CATEGORY_LAUNCHER);
                clickedIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                clickedIntent.putExtra("Action", "Resume");
                PendingIntent clickedPendingIndent = PendingIntent.getActivity(PlayingActivity.this, 2, clickedIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                // Create Channel
                String CHANNEL_ID = "StreamTube:Playing";
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                    NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, NotificationManager.IMPORTANCE_LOW);
                    channel.setDescription("Control StreamTube behaviour");
                    NotificationManager notificationManager = getSystemService(NotificationManager.class);
                    notificationManager.createNotificationChannel(channel);
                }

                // Notification Builder
                NotificationCompat.Builder builder = new NotificationCompat.Builder(PlayingActivity.this, CHANNEL_ID)
                        .setSmallIcon(R.drawable.ic_volume_up_black_48dp)
                        .setContentTitle(name)
                        .setContentText("StreamTube is playing")
                        .setTicker("Now Streaming from YouTube")
                        .setOngoing(true)
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setContentIntent(clickedPendingIndent)
                        .addAction(R.drawable.ic_pause_black_48dp, "Pause/Play", togglePendingIntent)
                        .addAction(R.drawable.ic_stop_black_48dp, "Stop", stopPendingIntent);

                Notification notifications = builder.build();
                NotificationManager notificationManger =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManger.notify(1, notifications);
                isNotification = true;
            }

            public void onServiceDisconnected(ComponentName className) {
            }

        };
        bindService(new Intent(PlayingActivity.this,
                        KillService.class), serviceConnection,
                Context.BIND_AUTO_CREATE);

    }
}
