package be.thomasave.streamyoutube;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import java.io.File;
import java.util.*;


public class DownloadsActivity extends AppCompatActivity {

    ListView output;
    List<String> listOfFiles = new ArrayList<String>();
    private static final int PERMISSION_REQUEST_CODE = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_downloads);
        setlist();
    }

    public void setlist(){
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            int result = getBaseContext().checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (result != PackageManager.PERMISSION_GRANTED) {
                this.requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
                return;
            }
        }
        File[] files = Environment.getExternalStoragePublicDirectory (Environment.DIRECTORY_MUSIC +"/StreamTube/").listFiles();
        List<File> fileList;
        if (files != null) {
            fileList = Arrays.asList(files);
        } else {
            fileList = new ArrayList<>();
        }

        Collections.sort(fileList, new Comparator<File>() {
            @Override
            public int compare(File file1, File file2) {
                long k = file1.lastModified() - file2.lastModified();
                if (k > 0){
                    return 1;
                } else if (k == 0){
                    return 0;
                } else{
                    return -1;
                }
            }
        });

        for (File file: fileList) {
            listOfFiles.add(file.getName().replace(".mp3", ""));
        }
        output = (ListView) findViewById(R.id.listView);

        if (fileList.size() == 0) {
            output.setAdapter(null);
        } else {
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.items, listOfFiles);
            output.setAdapter(adapter);
            output.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent i = new Intent(DownloadsActivity.this, PlayingActivity.class);
                    SharedPreferences sharedpreferences = getSharedPreferences("My_Prefs", Context.MODE_PRIVATE);
                    SharedPreferences.Editor mEdit1 = sharedpreferences.edit();
                    mEdit1.putString("name", listOfFiles.get(position));
                    mEdit1.apply();
                    i.putExtra("exists", "yes");
                    startActivity(i);
                    finish();
                }
            });
            registerForContextMenu(output);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                setlist();
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Please enable the 'Storage' permission. Otherwise, we can't access the downloaded files.")
                        .setTitle("Permission not granted");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        DownloadsActivity.this.requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        DownloadsActivity.this.finish();
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        }
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        if (v.getId()==R.id.listView) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.menu_list, menu);
        }
    }

    @Override
    public boolean onContextItemSelected(final MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch(item.getItemId()) {
            case R.id.delete:
                AlertDialog.Builder builder1 = new AlertDialog.Builder(DownloadsActivity.this);
                builder1.setMessage("Are you sure you want to delete this file?");
                builder1.setCancelable(false);
                builder1.setPositiveButton(android.R.string.yes,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int Clickid) {
                                AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
                                String filePath = Environment.getExternalStorageDirectory()+"/"+Environment.DIRECTORY_MUSIC +"/StreamTube/"+ listOfFiles.get(info.position) + ".mp3";
                                File file = new File(filePath);
                                file.delete();
                                setlist();
                                dialog.cancel();
                            }
                        });
                builder1.setNegativeButton(android.R.string.no,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int Clickid) {
                            dialog.cancel();
                            }
                        });
                AlertDialog alert11 = builder1.create();
                    alert11.show();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }
}
