package be.thomasave.streamyoutube;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class NotificationButtonIntentService extends IntentService {

    public static final String Action_Stop = "stop";
    public static final String Action_Toggle = "toggle";


    public NotificationButtonIntentService() {
        super("");
    }

    public NotificationButtonIntentService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String action = intent.getAction();
        if (action != null) {
           if (action.equals(Action_Toggle)) {
               PlayingActivity.play();
           } else if (action.equals(Action_Stop)) {
               NotificationManager notificationManger =
                       (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
               notificationManger.cancel(01);
               PlayingActivity.isNotification = false;
               PlayingActivity.stop();
           }
        }
    }
}
