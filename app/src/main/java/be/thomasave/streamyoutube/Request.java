package be.thomasave.streamyoutube;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.net.HttpURLConnection;
import java.net.URLEncoder;
import java.net.URL;

public class Request extends AppCompatActivity {

    ProgressDialog progress;
    String name, requestquality;
    String url = "empty";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences sharedpreferences = getSharedPreferences("My_Prefs", Context.MODE_PRIVATE);
        url = sharedpreferences.getString("url","empty");
        SharedPreferences getoption = PreferenceManager.
                getDefaultSharedPreferences(getBaseContext());
        requestquality = getoption.getString("Quality","192");
        Log.v("Quality: ",requestquality);
        if (!url.contains("empty")) {
            SharedPreferences.Editor mEdit1 = sharedpreferences.edit();
            mEdit1.putString("url", "empty");
            mEdit1.commit();
            Random r = new Random();
            int id = r.nextInt(999999999 - 1) + 1;
            Log.v("Id: ", "" + id);
            progress = ProgressDialog.show(this, "",
                    "Preparing file, please wait...", true);

            RequestTitleTask requestTitleTask = new RequestTitleTask(url,id, requestquality);
            requestTitleTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void) null);
        } else {
            // Get intent, action and MIME type
            Intent intent = getIntent();
            String action = intent.getAction();
            String type = intent.getType();

            if (Intent.ACTION_SEND.equals(action) && type != null) {
                if ("text/plain".equals(type)) {
                    handleSendText(intent);
                }
            } else {
                error();
            }
        }
    }

    void handleSendText(Intent intent) {
        String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
        if (sharedText != null) {
            Random r = new Random();
            int id = r.nextInt(999999999 - 1) + 1;
            Log.v("Id: ", "" + id);
            progress = ProgressDialog.show(this, "",
                    "Preparing file, please wait...", true);

            RequestTitleTask requestTitleTask = new RequestTitleTask(sharedText,id, requestquality);
            requestTitleTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void) null);
        }
    }

    void error() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(Request.this);
        builder1.setMessage("Please only use a URL to a youtube video.");
        builder1.setCancelable(false);
        builder1.setNeutralButton(android.R.string.ok,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        finish();
                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

public class RequestDownloadTask extends AsyncTask<Void, Void, Boolean> {

    String url, quality;
    int id;

    RequestDownloadTask(String youtubeurl, int requestid, String requestquality) {
        url = youtubeurl;
        id = requestid;
        quality = requestquality;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        Log.v("streamyoutube", "Starting download");
        try {
            URL my_url = new URL("http://streamtube.thomasave.be/requestdownload/");
            HttpURLConnection httpConn = (HttpURLConnection) my_url.openConnection();
            httpConn.setRequestMethod("POST");
            httpConn.setDoInput(true);
            httpConn.setDoOutput(true);
            httpConn.setReadTimeout(20000000);
            httpConn.setConnectTimeout(20000000);

            OutputStream outputStream = httpConn.getOutputStream();
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream);
            BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);
            String parameters = "id=" + id + "&url=" + URLEncoder.encode(url, "UTF-8") + "&quality=" + quality;
            Log.d("Parameters", parameters);
            bufferedWriter.write(parameters);
            bufferedWriter.flush();
            bufferedWriter.close();
            outputStream.close();

            InputStream inputStream = httpConn.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String line = bufferedReader.readLine();
            bufferedReader.close();
            inputStream.close();

            Log.d("Http Post Response:", line);

            httpConn.disconnect();

            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    protected void onPostExecute(final Boolean success) {
        progress.dismiss();

        if (success) {
            Log.v("Exists: ","no");
            SharedPreferences sharedpreferences = getSharedPreferences("My_Prefs", Context.MODE_PRIVATE);
            SharedPreferences.Editor mEdit1 = sharedpreferences.edit();
            mEdit1.putInt("id", id);
            mEdit1.commit();
            Intent i = new Intent(Request.this, PlayingActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            i.putExtra("id", id);
            i.putExtra("exists","no");
            startActivity(i);
            finish();
        } else {
            AlertDialog.Builder builder1 = new AlertDialog.Builder(Request.this);
            builder1.setMessage("Failed");
            builder1.setCancelable(false);
            builder1.setNeutralButton(android.R.string.ok,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            finish();
                        }
                    });
            AlertDialog alert11 = builder1.create();
            alert11.show();
        }
    }

    @Override
    protected void onCancelled() {
    }
}
    public class RequestTitleTask extends AsyncTask<Void, Void, Boolean> {

        String url, quality;
        int id;

        RequestTitleTask(String youtubeurl, int youtubeid, String requestquality) {
            url = youtubeurl;
            id = youtubeid;
            quality = requestquality;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                URL my_url = new URL("http://streamtube.thomasave.be/requesttitle/");
                HttpURLConnection httpConn = (HttpURLConnection) my_url.openConnection();
                httpConn.setRequestMethod("POST");
                httpConn.setDoInput(true);
                httpConn.setDoOutput(true);
                httpConn.setReadTimeout(20000000);
                httpConn.setConnectTimeout(20000000);

                OutputStream outputStream = httpConn.getOutputStream();
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream);
                BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);
                String parameters = "url=" + URLEncoder.encode(url, "UTF-8");
                bufferedWriter.write(parameters);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();

                InputStream inputStream = httpConn.getInputStream();
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String line = bufferedReader.readLine();
                bufferedReader.close();
                inputStream.close();

                Log.d("Http Post Response:", line);

                httpConn.disconnect();

                SharedPreferences sharedpreferences = getSharedPreferences("My_Prefs", Context.MODE_PRIVATE);
                SharedPreferences.Editor mEdit1 = sharedpreferences.edit();
                mEdit1.putString("name", line);
                mEdit1.commit();

                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(final Boolean success) {

            if (success) {
                if (isFileExists()){
                    Log.v("Exists: ","yes");
                    progress.dismiss();
                    Intent i = new Intent(Request.this, PlayingActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    i.putExtra("id", id);
                    i.putExtra("exists", "yes");
                    startActivity(i);
                    finish();
                } else {
                    RequestDownloadTask requestDownloadTask = new RequestDownloadTask(url,id, quality);
                    requestDownloadTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void) null);
                }

            } else {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(Request.this);
                builder1.setMessage("Failed");
                builder1.setCancelable(false);
                builder1.setNeutralButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                finish();
                            }
                        });
                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        }

        @Override
        protected void onCancelled() {
        }
}
    private boolean isFileExists(){

        File folder1 = new File(Environment.getExternalStorageDirectory()+"/"+Environment.DIRECTORY_MUSIC +"/StreamTube/"+ name + ".mp3");
        return folder1.exists();


    }
}
