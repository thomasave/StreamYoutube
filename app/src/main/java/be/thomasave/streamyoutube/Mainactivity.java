package be.thomasave.streamyoutube;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Mainactivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button button = (Button) findViewById(R.id.button);
        final EditText editText = (EditText) findViewById(R.id.editText);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = editText.getText().toString();
                SharedPreferences sharedpreferences = getSharedPreferences("My_Prefs", Context.MODE_PRIVATE);
                SharedPreferences.Editor mEdit1 = sharedpreferences.edit();
                mEdit1.putString("url", url);
                mEdit1.commit();
                Intent i = new Intent(Mainactivity.this, Request.class);
                startActivity(i);
                finish();
            }
        });

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_activity_actions, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent intentabout = new Intent(this, settings.class);
            startActivity(intentabout);
            return true;
        }
        if (id == R.id.action_downloads) {
            Intent intentabout = new Intent(this, DownloadsActivity.class);
            startActivity(intentabout);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
