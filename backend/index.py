#!/usr/bin/env python3
from __future__ import unicode_literals
from datetime import datetime
from flask import Flask, send_from_directory, request
from threading import Thread
import glob
import os
import sys
import time
import youtube_dl


app = Flask(__name__, static_url_path='')
path = os.path.dirname(os.path.abspath(__file__))

# def clean():
#     while True:
#         for file in glob.glob("downloads/*"):
#             modified = datetime.utcfromtimestamp(os.path.getmtime(file))
#             hours = (datetime.now() - modified).seconds / (60 * 60)
#             if hours > 24:
#                 print("Removing file:", file)
#                 os.remove(file)
#         time.sleep(60*60*24)
#
# thread = Thread(target=clean)
# thread.start()

@app.route('/requestdownload/', methods=['POST'])
def download():
    quality = request.form.get('quality', '192')
    ydl_opts = {
    'outtmpl': path + '/downloads/' + request.form['id']+'.mkv',
    'format': 'best',
    'source_address': '0.0.0.0',
    'postprocessors': [{
        'key': 'FFmpegExtractAudio',
        'preferredcodec': 'mp3',
        'preferredquality': '192',
        }],
    }
    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
       ydl.download([request.form['url']])
    return 'ready'
    
@app.route('/requesttitle/', methods=['POST'])
def title():
    ydl_opts = {'source_address': '0.0.0.0'}
    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
      info_dict = ydl.extract_info(request.form['url'], download=False)
      video_title = info_dict.get('title', None)
      for i in '''!@#$?&{}\`<>*/"':+|=''':
          video_title = video_title.replace(i, "")
    return video_title

@app.route('/requestremoval/', methods=['POST'])
def removal():
    filename = path + '/downloads/' + request.form['id']+".mp3"
    if os.path.exists(filename):
        os.remove(filename)
    return 'success'
    
@app.route('/') 
def root():
    return """<!DOCTYPE HTML>
<form action="requestdownload/" method="post">
  Url: <input type="url" name="url"><br>
  Id: <input type="text" name="id"><br>
  Quality: 
  <select name="quality">
    <option value="192">192kbps</option>
    <option value="320">320kbps</option>
</select>
  <input type="submit" value="Submit">
</form>"""

@app.route('/tmp/<filename>')
def send_file(filename):
    return send_from_directory(path + '/downloads/', filename)
