import os
import sys

sys.path.append(os.path.dirname(__file__))

from index import app 

if __name__ == "__main__":
    app.run()
